import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import xo.Table;
import xo.Player;

class TestUnitTest {

	@BeforeAll
	static void setUpBeforeClass()  {
	}

	@AfterAll
	static void tearDownAfterClass() {
	}

	@BeforeEach
	void setUp() {
	}

	@AfterEach
	void tearDown() {
	}

	@Test
	void test() {
		fail("Not yet implemented");
	}
	
	public void testRow1Win() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.setRowCol(1, 1);
		table.setRowCol(1, 2);
		table.setRowCol(1, 3);
		assertEquals(true,table.checkWin());		
	}
	
	public void testSwitchPlayer() {
		Player o = new Player('o');
		Player x = new Player('x');
		Table table = new Table(o,x);
		table.switchTurn();
		assertEquals('x',table.getCurrentPlayer().getName());		
	}
	

}
